# budget-sharing

L'objectif du projet sera de créer une application front end, en Typescript sans framework, permettant de calculer et partager un budget entre ami⋅es

## Fonctionnalités

* Créer un groupe de dépenses
* Entrer des dépenses dans un groupe (Ex: `Billet de Train 25€ - Marc`, `Sac de pomme pour le pique nique 2.99€ - Hamza`)
* Consulter la liste et le total des dépenses, filtrer par personnes et montant (supérieur/inférieur à...)
* Clôturer un groupe ce qui permet d'avoir le total des dépenses, le total par personne et qui doit donner combien à qui pour équilibrer (et pouvoir cocher quand on a déjà remboursé)

## Réalisation

* L'application doit marcher intégralement sans interface graphique
* Utiliser des classes et des interfaces pour créer le fonctionnement internet de l'application
* Réaliser une interface graphique en HTML via le DOM qui utilisera les classes "modèles" pour fonctionner